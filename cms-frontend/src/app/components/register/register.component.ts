import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {User} from '../../model/User';
import {HttpService} from '../../services/http.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  disableLogin = true;
  model = new User();
  registerForm = this.fb.group({
    email: new FormControl('', [Validators.required, Validators.pattern('^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$')]),
    mobileNumber: new FormControl('', [Validators.required, Validators.minLength(10),
      Validators.maxLength(11), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]),
    firstName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    lastName: new FormControl('', [Validators.required, Validators.pattern('^[a-zA-Z \-\']+')]),
    dob: new FormControl('', null),
    gender: new FormControl('', null)
  });
  constructor(private fb: FormBuilder, public httpService: HttpService) {}
  ngOnInit() {
  }
  get mobileNumber() { return this.registerForm.get('mobileNumber'); }
  get firstName() { return this.registerForm.get('firstName'); }
  get lastName() { return this.registerForm.get('lastName'); }
  get email() { return this.registerForm.get('email'); }
  get dob() { return this.registerForm.get('dob'); }
  get gender() { return this.registerForm.get('gender'); }
  public onFormSubmit() {
    if (this.registerForm.valid) {
        this.httpService.createUser(this.model).subscribe(
          data => {
            this.registerForm.disable();
            this.disableLogin = false;
          }
        );
    }
  }
}

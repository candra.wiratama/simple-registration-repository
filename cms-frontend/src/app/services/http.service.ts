 import { Injectable } from '@angular/core';
 import {HttpClient, HttpHeaders} from '@angular/common/http';
 import {Observable, throwError} from 'rxjs';
 import {User} from '../model/User';
 import {catchError, retry} from 'rxjs/operators';

 @Injectable({
  providedIn: 'root'
 })
export class HttpService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };
  constructor(private httpClient: HttpClient) { }
  createUser(user): Observable<User> {
    return this.httpClient.post<User>('http://localhost:8080/api/v1/users', JSON.stringify(user), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    if (error.statusText === 'Unknown Error') {
      window.alert('Internal server error occured.');
    } else {
      window.alert('Failed to save user. Data alreay exist.');
    }
    return throwError(errorMessage);
  }
}

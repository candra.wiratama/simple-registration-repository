export class User {
  public mobileNumber: string;
  public firstName: string;
  public lastName: string;
  public dob: Date;
  public gender: string;
  public email: string;

  constructor() {}
}

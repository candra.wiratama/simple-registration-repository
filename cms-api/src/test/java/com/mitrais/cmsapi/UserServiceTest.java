package com.mitrais.cmsapi;

import com.mitrais.cmsapi.Model.User;
import com.mitrais.cmsapi.controller.UserController;
import com.mitrais.cmsapi.repository.UserRepository;
import com.mitrais.cmsapi.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@WebMvcTest(UserController.class)

public class UserServiceTest {
    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @Test
    public void it_should_return_created_user() throws Exception {

        User user = new User();
        user.setMobileNumber("081218374333");
        user.setFirstName("candra");
        user.setLastName("kumoro");
        user.setDob(new Date());
        user.setGender("Male");
        user.setEmail("candra@gmail.com");
        when(userRepository.save(any(User.class))).thenReturn(user);
        assertTrue(userService.save(user));
    }

}

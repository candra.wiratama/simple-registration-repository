package com.mitrais.cmsapi;

import com.mitrais.cmsapi.Model.User;
import com.mitrais.cmsapi.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired
    TestEntityManager entityManager;
    @Autowired
    UserRepository userRepository;

    @Test
    public void save_user_OK() throws Exception {

        User user = new User();
        user.setMobileNumber("081218374333");
        user.setFirstName("candra");
        user.setLastName("kumoro");
        user.setDob(new Date());
        user.setGender("Male");
        user.setEmail("candra@gmail.com");

        user = entityManager.persistAndFlush(user);
        assertThat(userRepository.findById(user.getId()).get()).isEqualTo(user);
    }

}

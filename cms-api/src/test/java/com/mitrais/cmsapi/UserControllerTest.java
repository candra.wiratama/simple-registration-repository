package com.mitrais.cmsapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mitrais.cmsapi.Model.User;
import com.mitrais.cmsapi.controller.UserController;
import com.mitrais.cmsapi.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void it_should_return_created_user() throws Exception {

        User user = new User();
        user.setMobileNumber("081218374333");
        user.setFirstName("candra");
        user.setLastName("kumoro");
        user.setDob(new Date());
        user.setGender("Male");
        user.setEmail("candra@gmail.com");
        when(userService.save(any(User.class))).thenReturn(true);
        mockMvc.perform(post("/api/v1/users")
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void it_shouldnot_return_created_user() throws Exception {

        User user = new User();
        user.setMobileNumber("081218374333");
        user.setFirstName("candra");
        user.setLastName("kumoro");
        user.setDob(new Date());
        user.setGender("Male");
        user.setEmail("candra@gmail.com");
        boolean resultA = userService.save(user);

        user = new User();
        user.setMobileNumber("081218374333");
        user.setFirstName("candra");
        user.setLastName("kumoro");
        user.setDob(new Date());
        user.setGender("Male");
        user.setEmail("candra@gmail.com");
        resultA = userService.save(user);

        mockMvc.perform(post("/api/v1/users")
                .content(mapper.writeValueAsString(user))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

}

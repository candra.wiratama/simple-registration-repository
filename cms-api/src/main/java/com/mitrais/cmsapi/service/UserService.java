package com.mitrais.cmsapi.service;

import com.mitrais.cmsapi.Model.User;
import com.mitrais.cmsapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public boolean save(User user) {
        Optional<User> existingMobileNo = userRepository.findByMobileNumber(user.getMobileNumber());
        Optional<User> existingEmail = userRepository.findByEmail(user.getEmail());
        if(existingMobileNo.isPresent() || existingEmail.isPresent()) {
            return false;
        }
        userRepository.save(user);
        return true;
    }
}

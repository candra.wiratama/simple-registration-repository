Specification:

cms-frontend technology used:

1. angular 7
2. angular material

cms-api technology used:

1. spring boot
2. Hsql DB
3. Maven
4. Hibernate

How's to:


1. git clone https://gitlab.com/candra.wiratama/simple-registration-repository.git
2. import each project to your favourite IDEA
3. setting and running project

    3a. cms-frontend
        
        a. execute npm install from cmd or terminal from IDEA if any
        b. execute ng serve
        c. goto localhost:4200/register at your browser

    3b. cms-api

        a. execute mvn clean test spring-boot:run
        b. the url api would be available at localhost:8080/api/v1/users
        